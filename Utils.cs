﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Serialize
{
    public interface IConverter<T>
        where T : class
    {
        string Serialize(T @object);
        T Deserialize(string str);
    }

    public class CaptionAttribute : Attribute
    {
        public string Caption { get; set; }
        public CaptionAttribute(string name)
        {
            Caption = name;
        }
    }

}
