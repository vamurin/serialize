﻿using System;
using System.Reflection;
using System.Text;

namespace Serialize
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            var model = new Model { Id = "10", Value = "Пример" };

            Console.WriteLine("Serialized Model = " + model.Serialize(model));
            Console.WriteLine("");
            string str = "Идентификатор:5;Value:Контрпример";
            
            Console.WriteLine("Serialized string = " + str);
            model = model.Deserialize(str);
            Console.WriteLine(String.Format("Deserialized Model = {{ Id  = {0}, Value = {1} }}", model.Id, model.Value));

            Console.Read();
        }
    }   
}
