﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Serialize
{
    public class Model : IConverter<Model>
    {
        [Caption("Идентификатор")]
        public string Id { get; set; }
        public string Value { get; set; }

        public Model Deserialize(string str)
        {
            Model model = new Model();

            var properties = typeof(Model).GetProperties();

            string[] substrs = str.Split(";");

            foreach (string substr in substrs)
            {
                string[] strProperties = substr.Split(":");

                foreach (var property in properties)
                {
                    if (property.IsDefined(typeof(CaptionAttribute), false) && property.GetCustomAttribute<CaptionAttribute>().Caption == strProperties[0])
                        property.SetValue(model, strProperties[1]);
                    else if (!property.IsDefined(typeof(CaptionAttribute), false) && property.Name == strProperties[0])
                        property.SetValue(model, strProperties[1]);
                }
            }

            return model;
        }

        public string Serialize(Model @object)
        {
            var properties = typeof(Model).GetProperties();

            string result = "";

            bool first = true;

            foreach (var property in properties)
            {
                var value = property.GetValue(@object);
                var name = property.Name;
                if (property.IsDefined(typeof(CaptionAttribute), false))
                    name = property.GetCustomAttribute<CaptionAttribute>().Caption;

                if (!first)
                    result += ";";
                result += String.Format("{0}:{1}", name, value);

                first = false;
            }

            return result;
        }
    }
}
